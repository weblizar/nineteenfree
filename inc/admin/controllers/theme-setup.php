<?php
defined( 'ABSPATH' ) or die();

function nineteen_theme_setup() {

	/*
   * Make theme available for translation.
   * Translations can be filed in the /languages/ directory.
   * If you're building a theme based on Theme Palace, use a find and replace
   * to change 'nineteen' to the name of your theme in all the template files.
   */
    load_theme_textdomain( 'nineteen' );

	/* Register custom menu. */
	register_nav_menu( 'primary', __( 'Primary Menu', 'nineteen' ) );

	/* Gutenberg */
	add_theme_support( 'wp-block-styles' );
	add_theme_support( 'align-wide' );
	add_theme_support( 'customize-selective-refresh-widgets' );

	/* Add editor style. */
	add_theme_support( 'editor-styles' );
	add_theme_support( 'dark-editor-style' );

	/* Load editor style. */
	add_editor_style();

	/* Add default posts and comments RSS feed links to head. */
	add_theme_support( 'automatic-feed-links' );

	/* Enable support for Post Thumbnails. */
	add_theme_support( 'post-thumbnails' );

	/* Add title tag support. */
	add_theme_support( 'title-tag' );


	/* Create upload dir. */
	wp_upload_dir();

	/* Enable support for Woocommerce */
  	add_theme_support( 'woocommerce' );
  	add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );

    /* Enable support for HTML5 */
    add_theme_support( 'html5', array(
	    'comment-form',
	    'comment-list',
	    'gallery',
	    'caption',
	  ) );

     $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );

    add_theme_support( 'custom-background' );

    if ( ! isset( $content_width ) ) $content_width = 900;

}

function nineteen_widgets_init() {

    /*sidebar*/
    register_sidebar( array(
      'name'          => __( 'Sidebar', 'nineteen' ),
      'id'            => 'sidebar-primary',
      'description'   => __( 'The primary widget area', 'nineteen' ),
      'before_widget' => '<div id="%1$s" class="blog-sidebar-widgets %2$s">',
	  'after_widget'  => '</div>',
	  'before_title'  => '<h4 class="widgets-title"> <span>',
	  'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
      'name'          => __( 'Footer Widgets', 'nineteen' ),
      'id'            => 'footer-widget-area',
      'description'   => __( 'footer widget area', 'nineteen' ),
      'before_widget' => '<div id="%1$s" class="col-md-4 widgets-col %2$s">',
	    'after_widget'  => '</div>',
	    'before_title'  => '<h3 class="widgets-title">',
	    'after_title'   => '</h3>',
    ) ); 
}

/** Theme setup **/
add_action( 'after_setup_theme', 'nineteen_theme_setup', 5 );

/** Theme **/
add_action( 'widgets_init', 'nineteen_widgets_init' );

//Plugin Recommend
add_action( 'tgmpa_register','nineteen_plugin_recommend' );
function nineteen_plugin_recommend(){
	$plugins = array(
  	array(
        'name'     => __('weblizar-companion','nineteen'),
        'slug'     => 'weblizar-companion',
        'required' => false,
    )
	);
  tgmpa( $plugins );
}