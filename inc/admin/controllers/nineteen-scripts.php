<?php
defined( 'ABSPATH' ) or die();

function nineteen_fonts_url() {
    $fonts_url       = '';
    $font_families   = array();
    $font_families[] = 'Montserrat:400,600,800';
    $font_families[] = 'Open+Sans:400,600,800';

    $query_args = array(
        'family' => urlencode( implode( '|', $font_families ) ),
    );

    $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

    return esc_url_raw( $fonts_url );
}

function nineteen_scripts_head() {
    /* Css files */
    wp_enqueue_style( 'nineteen-font', esc_url(nineteen_fonts_url()), array(), null );
    wp_enqueue_style( 'nineteen-main-style', get_template_directory_uri(). '/assets/css/main_style.css' );
    wp_enqueue_style( 'nineteen-responsive-js', get_template_directory_uri(). '/assets/css/responsive.css' );
    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri(). '/assets/css/bootstrap.css' );
    wp_enqueue_style( 'animations', get_template_directory_uri(). '/assets/css/animations.css' );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri(). '/assets/font-awesome-5.11.2/css/all.css' );
    wp_enqueue_style( 'swiper', get_template_directory_uri(). '/assets/css/swiper.css' );
    wp_enqueue_style('nineteen-style-sheet', get_stylesheet_uri());
}

function nineteen_scripts_footer() {
    /* Js files */       
    wp_enqueue_script( 'nineteen-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array('jquery'), true, true );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri(). '/assets/js/bootstrap.js', array( 'jquery' ), true, true );
    wp_enqueue_script( 'popper', get_template_directory_uri(). '/assets/js/popper.js', array( 'jquery' ), true, true );
    wp_enqueue_script( 'animations', get_template_directory_uri(). '/assets/js/animations.js', array( 'jquery' ), true, true );
    wp_enqueue_script( 'swiper', get_template_directory_uri(). '/assets/js/swiper.js', array( 'jquery' ), true, true );
    wp_enqueue_script( 'nineteen-custom-script', get_template_directory_uri(). '/assets/js/custom-script.js', array( 'jquery' ), true, true );
}

/* Enqueue Theme Style and Script Files in header */
add_action( 'wp_enqueue_scripts', 'nineteen_scripts_head' );

/* Enqueue Theme Style and Script Files in Footer */
add_action( 'wp_enqueue_scripts', 'nineteen_scripts_footer' );