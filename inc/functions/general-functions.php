<?php 
defined( 'ABSPATH' ) or die();

/* get post publish date */
if ( ! function_exists( 'nineteen_theme_get_post_publish_date' ) ) {
	function nineteen_theme_get_post_publish_date( $post_id ) {
		$posts = get_posts( array( 'post_type' => 'post', 'post__in' => array( $post_id ), 'numberposts' => -1 ) ); //Get all published posts
		foreach ( $posts as $post ) {
		    return esc_html(get_the_time( 'F', $post->ID )).' '.esc_html(get_the_time( 'd', $post->ID )).', '.esc_html(get_the_time( 'Y', $post->ID ));
		}
	}
}

/* Get the pagination */
if ( ! function_exists( 'nineteen_theme_get_the_pagination' ) ) {
	function nineteen_theme_get_the_pagination() {
		?>
	    <div class="text-center wl-theme-pagination">
	        <?php the_posts_pagination(); ?>
	        <div class="clearfix"></div>
	    </div>
	<?php
	}
}

//comment-reply js //
function nineteen_enqueue_comments_reply() {

    if( is_singular() && comments_open() && ( get_option( 'thread_comments' ) == 1) ) {
        // Load comment-reply.js (into footer)
        wp_enqueue_script( 'comment-reply', 'wp-includes/js/comment-reply', array(), false, true );
    }
}
add_action(  'wp_enqueue_scripts', 'nineteen_enqueue_comments_reply' );

add_action('wp_enqueue_scripts', 'nineteen_custom_css');
function nineteen_custom_css()
{
    $output = '';
    
    $output     .='
    .navbar-brand img{
        height:'.esc_attr(get_theme_mod('nineteen_logo_heigth','70')).'px;
        width:'.esc_attr(get_theme_mod('nineteen_logo_width','180')).'px;
    }';

    $output     .='
    .page-blog{
        background-color:#'.get_background_color().';
    }';
    //custom css
    $custom_css = get_theme_mod('custom_css') ; 
    if (!empty ($custom_css)) {
        $output .= get_theme_mod('custom_css') . "\n";
    }

    wp_register_style('custom-header-style', false);
    wp_enqueue_style('custom-header-style', get_template_directory_uri() . '/assets/css/custom-header-style.css');
    wp_add_inline_style('custom-header-style', $output);
}

?>