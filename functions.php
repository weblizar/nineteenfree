<?php
/**
 * Nineteen Theme functions and definitions
 *
 * @package nineteen
 */

defined( 'ABSPATH' ) or die();

/* Get the plugin */
if ( ! function_exists( 'nineteen_theme_is_companion_active' ) ) {
	function nineteen_theme_is_companion_active() {
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    	if ( is_plugin_active(  'weblizar-companion/weblizar-companion.php' ) ) {
    		return true;
    	} else {
    		return false;
    	}
	}
}

require( get_template_directory() . '/inc/functions/breadcrumb/nineteen-custom-breadcrumb.php' );
require( get_template_directory() . '/inc/functions/comment/nineteen-comment.php' );
require( get_template_directory() . '/inc/functions/menu/nineteen-nav-walker.php' );
require( get_template_directory() . '/inc/functions/menu/default-menu-walker.php' );
require( get_template_directory() . '/inc/admin/controllers/nineteen-scripts.php' );
require( get_template_directory() . '/inc/functions/general-functions.php' );
require(get_template_directory() . '/inc/functions/custom-header.php');
require( get_template_directory() . '/inc/admin/controllers/theme-setup.php' );
require( get_template_directory() . '/inc/admin/class-tgm-plugin-activation.php' );