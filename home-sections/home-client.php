<?php
if ( nineteen_theme_is_companion_active() ) {
	$client_home = absint(get_theme_mod( 'client_home','1' ));
	if ( $client_home == "1" ) {
        /* Executes the action for clients section hook named 'wl_companion_client' */
        do_action( 'wl_companion_client');
    }
}