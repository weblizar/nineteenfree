<?php 
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * Learn more: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nineteen
 */
?>
<!--footer-strart-->
<footer>
    <div class="footer_inner">
        <div class="margin-80 clearfix"> </div>
        <div class="container">
            <!--footer-widgets-->
            <div class="footer-widgets animate " data-anim-type="fadeInDownLarge" data-anim-delay="400">
                <div class="row">
                    <?php 
                        if ( is_active_sidebar( 'footer-widget-area' ) ) { 
                            dynamic_sidebar( 'footer-widget-area' ); 
                        } 
                    ?>
                </div>
            </div>
        </div>
        <!--//footer-widgets-->
        <!--coppy-right-->
        <div class="margin-80 xs-hiddn clearfix"> </div>
        <div class="coppy-right text-center">
            <?php if ( nineteen_theme_is_companion_active() ) { ?>
                <p class="copy_nineteen">
                    <i class="fa fa-copyright"></i> 
                    <?php echo esc_html( get_theme_mod( 'nineteen_footer_customization', __('Powered by WordPress','nineteen') ) ); ?> 
                    <i class="fa fa-heart"></i>
                    <a href="<?php echo esc_url( get_theme_mod( 'nineteen_deve_link' ) ); ?>"> 
                        <?php echo esc_html( get_theme_mod( 'nineteen_develop_by' ) ); ?> 
                    </a>
                </p>
            <?php } ?>
        </div>
        <!--./coppy-right-->
    </div>
</footer>
</div>
<!-- Return to Top -->
<?php if ( nineteen_theme_is_companion_active() ) { 
    $nineteen_return_top = absint(get_theme_mod( 'nineteen_return_top', '1' ));
    if ( $nineteen_return_top == "1" ) { ?>
        <a href="javascript:" id="return-to-top"><i class="fas fa-arrow-alt-circle-up h4"></i></a>
    <?php 
    } 
}
wp_footer();
?>
</body>
</html>