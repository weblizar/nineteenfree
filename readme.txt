=== Nineteen ===
Contributors: weblizar
Tags: two-columns, custom-menu, right-sidebar, custom-background, featured-image-header, sticky-post, threaded-comments, featured-images, blog, entertainment, e-commerce
Requires at least: 4.0
Tested up to: 5.5
Stable tag:  1.1.3
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Free WordPress theme can be used for different kinds of websites.

== Description ==
Nineteen is responsive , multi-page WordPress theme for Business. Theme is built with leading CSS framework which adapts all leading devices , browsers and Page Builders. This theme is compatible with all leading browsers and all popular wordpress plugins like WPML / woo-commerce / Elemantor / contact form 7 etc.

== Resources ==

Nineteen WordPress Theme, Copyright 2019 weblizar
Nineteen is distributed under the terms of the GNU GPL.

Theme is Built using the following resource bundles.

= Theme-fonts =

@ 2004, Version 2.0, http://www.apache.org/licenses/, Apache License                        

= BUNDELED JS = 

    Bootstrap v4.0.0 (https://getbootstrap.com)
    * Copyright 2011-2018 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
    * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

    Swiper 4.2.0
	* Most modern mobile touch slider and framework with hardware accelerated transitions
	* http://www.idangero.us/swiper/
	* Copyright 2014-2018 Vladimir Kharlampidi
	* Released under the MIT License
	* Swiper jQuery library @ 2018, Vladimir Kharlampidi,  http://www.idangero.us/swiper/, MIT License

	Animations v1.0, 
	* Copyright 2014, Joe Mottershaw, https://github.com/joemottershaw/ , MIT License

	Popper Js
	* Copyright (C) Federico Zivolo 2018
 	* Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).

	* Custom script @ 2019, Weblizar, GPL

= BUNDELED CSS =

	Font Awesome Free 5.11.2 by @fontawesome - https://fontawesome.com
	* License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)

	Bootstrap v4.1.3 (https://getbootstrap.com/)
	* Copyright 2011-2018 The Bootstrap Authors
	* Copyright 2011-2018 Twitter, Inc.
 	* Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

	Swiper 4.2.0
	* http://www.idangero.us/swiper/ 
	* Copyright 2014-2018 Vladimir Kharlampidi
	* Released under the MIT License

	Animations v1.0, 
	* Copyright 2014, Joe Mottershaw, https://github.com/joemottershaw/ , MIT License

	* Responsive CSS, @ 2019, weblizar, GPL

	* main_style CSS, @ 2019, weblizar, GPL 

= Photo Credit =
	= Screenshot =
	* https://pxhere.com/en/photo/764684
	All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)

	= Banner =
	* https://pxhere.com/en/photo/971635
	All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)
 

== Changelog ==

= 1.1.3=
* Add Accessebility
* Fontawesome update
* Update according to new guidline
* Screenshot Update

= 1.1.2=
* Minor fix


= 1.1.1
* accessibility-ready support Roll back.
* Remove Inline Css & Js code
* Bug Fix ( Escaping functions )
* Code Optimize

= 1.1.0 
* accessibility-ready support added.

= 1.0.9 
* Option added for return top button enable/disable.
* Changes in admin notice.

= 1.0.8 [15-04-2019] =
* implemented new feature to add scripts in header and footer

= 1.0.7 [28-03-2019] =
* bugs fixed
* Made admin notice dismiss-able

= 1.1 [09-03-2019] =
* Added post fullwidth layout
* Added post right sidebar layout